# comment example
# sales_token = "LARGETOKEN"
// this kind of comment
token = "EJGzG8eHkve85iK37N48EthJGidpi4eSKaVwWr"
access_key  = "bxv3otWYZe8oPnawGKRWKBa9fDvD56eK7fMxY2"
secret_key  = "mT4yqCPm7Ayn9vDGfKsQPUR5XCvLRiNGgho7yB"
token_vault = "tDPGkk4Z9HjFyZihGmG47SP74QAoBAnMDaFMSX"
secretnumber = 456.32

### more comments
array = ["one", "two", "three"]
array2 = [
    "one",
    "two",
    "three"
]
# we dont support this
#array4 = 
#[
#    "sick",
#    "dead",
#    "dying"
#]
/*
a multiline comment

some = none
ddd
*/
array3 = [one, two, three]
web_amis = {
  us-east-2 = "ami-0dacb0c129b49f529"
  us-west-2 = "ami-00068cd7555f543d5"
}
andy=without quotes
bob = without quotes
stevedave = {
  one = "two"
  three = four
}
